# Quickstart
---
This guide will show you how to authorize against our API and invite people to an event. You will need to:
- Authorize using OAuth2.
- Get a list of Events.
- Invite Team Members to the chosen event.

## Authorize using OAuth2
Using the credentials provided to your organization, navigate to:
```
 https://blerter.auth0.com/authorize?audience=user&response_type=code&client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&nonce={CRYPTOGRAPHIC_NONCE}
```
Then exchange the code for a token:
```
curl --request POST \
--url 'https://blerter.auth0.com/oauth/token' \
--header 'content-type: application/json' \
--data '{"grant_type":"authorization_code","client_id":"CLIENT_ID","client_secret": "CLIENT_SECRET","code": "CODE","redirect_uri":"REDIRECT_URI"}'
```
The response should contain your `ACCESS_TOKEN` which must be attached as a bearer token in all following requests to the API. E.g.
```
 curl --request GET \
 --url https://api.cloudm.co.nz/public/v1/events \
 --header 'authorization: Bearer ACCESS_TOKEN' \
 --header 'content-type: application/json'
 ```

## Get a List of Events
A list of events for the authenticated user can be obtained via a HTTP GET request.
```
curl --request GET \
--url https://api.cloudm.co.nz/public/v1/events \
--header 'authorization: Bearer ACCESS_TOKEN' \
--header 'content-type: application/json'
```
The response will look something like:
```
{
    "itemCount": 2,
    "totalCount": 2,
    "items": 
    [
        {
            "id": 1,
            "name": "Fun Run"
        },
        {
            "id": 2,
            "name": "Another event"
        }
    ]
}
```
Here the ITEMS array contains two events. 

## Inviting a Team Member
A Team Member can be added to an event via a POST request against the specified event:
```
curl --request POST \
--url https://api.cloudm.co.nz/public/v1/event-invitations \
--header 'authorization: Bearer ACCESS_TOKEN' \
--header 'content-type: application/json' \
--data  \
'{
    "email": "​joeblogs@somewhere.com​",
    "eventId": EVENT_ID,
    "name": "Joe Bloggs",
    "jobTitle": "Coordinator"
}'
 ```
 The `EVENTID` matches one of those returned earlier. The result should look something like
```
{
    "errors": [],
    "success": true,
    "object": 
    {
        "id": 101,
        "email": "​joeblogs@somewhere.com​",
        "eventId": 1,
        "name": "Joe Bloggs",
        "jobTitle": "coordinator"
    }
}