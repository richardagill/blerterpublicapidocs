# Events
In terms on Blerter's API an Event looks something like [this](../../types/Event).

It serves a place to organize your documentation, Runsheets and Team.

You can get a list of all your events by performing a GET on the [events](../../routes/v1/events/get) endpoint.

Events cannot be created or modified by the API at the moment. Visit https://desktop.blerter.com in order to do this.


## Inviting Team members to an Event
You can invite members of you team to an event by performing a POST to the [event-invitations](../../v1/routes/event-invitations/post) endpoint.

You must supply their `email` and the `id` of the event you wish to join them to. Their `job-title` and `name` can also be (optionally) supplied. 

The request will send an email to the recipient with instructions on join the event using a link.

[EventInvitations](../../types/EventInvitation) once created with contain their own `id`. This allows you to updated an existing invitation on subsequent requests. You should not include it when creating an initial invitation.

The [Quickstart](./Quickstart) example illustrates how to get a list of events and then create an invitation.

