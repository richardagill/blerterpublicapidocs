# Common Concepts
---
Our API is based on REST.
- We use mostly use the GET, POST and DELETE verbs.
- *Most* requests should contain an `ACCESS_TOKEN` transmitted as a bearer-token in the request header.
- All POST bodies must be transmitted as JSON and the request header should indicate this with `content-type: application/json`.
- Responses are always returned as JSON.

## GET Requests
These are made when you need to view data. They will *never* modify it. The HTTP response will have a code of 200 when successful or something in the 400 or 500 range when an error occurs.

## POST Requests
These are made when you need to modify data. TThe HTTP response will have a code of 200 when *processed* by the server or something in the 400 or 500 range otherwise. A code of 200 does not necessarily indicate success however, only that the request has been processed. POST requests will *always* return a [ValidationResult](#ValidationResults) that will indicated the overall success or failure (and the reasons why) of the request.

## DELETE Requests
These are made when you need to remove data or deactivate a record. TThe HTTP response will have a code of 200 when *processed* by the server or something in the 400 or 500 range otherwise. Like POST requests they will always return a [ValidationResult](#ValidationResults) 

## Errors
When an error occurs your HTTP response will contain a code other than 200, in the 400 or 500 range. E.g.
```
RESPONSE CODE 404
{
    "message": "Method does not exist."
}
```

## Common Errors
The most common HTTP error codes encountered when using the API are:
- **400 Bad Request:** The syntax of request is not correct or a required parameter may have been omitted.
- **401 Unauthorized** The transmitted credentials are missing or do not have the required level of authorization to make the request. This is usually a problem with the `ACCESS_TOKEN` transmitted in the header. Check:
    - The token is present in the request header.
    - The token may be badly formatted.
    - The token may be stale and has expired.
    - The user that you are making the request on behalf of may not have sufficient privileges to make this request.
- **404 Not Found:** The resource is not present. Most often caused by calling an end-point that does not exist or using the wrong method/verb.
- **500 Internal error** Something unexpected has happened in Blerter. Contact our support if this persists.

## ValidationResults
You can expect a response body in this format whenever you make a *POST* or *DELETE* request. It's purpose is to inform you as to the overall success of the operation being preformed and reason(s) for failure if unsuccessful.
A successful operation will a response in the following format:
```
{
    "errors": [],
    "success": true,
    "object": 
    {
        "id": 101,
        "email": "​joeblogs@somewhere.com​",
        "eventId": 1,
        "name": "Joe Bloggs",
        "jobTitle": "coordinator"
    }
}
```
Conversely an unsuccessful operation will look like this:
```
{
    "errors": 
    [
        {
            "code": "InvalidValue",
            "field": "email",
            "message": "The email is missing or invalid"
        }
    ],
    "success": false,
    "object": null
}
```
Where:
`success`: A boolean value that indicates the overall success or failure of the operation.
- When `true` you can expect the `errors` array to be empty and the `object` field to be non-null.
- When `false` you can expect the `errors` array to contain one or more entries and the `object` field to be `null`.
`errors`: Contains an array of error objects. Each of which indicates a `code` and `message` that indicate the cause of failure. Some request failures may contain multiple errors, which can be useful to a client when indicating which elements are invalid. (Using the associated `field`)
`object`: Contains contextual data associated with the outcome of the operation.


## ListResults
You can expect a response body in this format whenever you make a *GET* request that contains multiple records.
It looks something like:
```
{
    "itemCount": 2,
    "totalCount": 2,
    "items": 
    [
        {
            "id": 1,
            "name": "Fun Run"
        },
        {
            "id": 2,
            "name": "Another event"
        }
    ]
}
```
Where:
`items`: An array of contextual data records. 
`itemCount`: The number of records present in the `items` array.
`totalCount`: The total number of records that are available when [Paging](#Paging) 

## Paging
When a ListResult would return large number of entries a client will expected to "page" through the result set by making multiple requests.
A `count` and `offset` parameter can be included to achieve this. E.g:
```
curl --request GET \
--url https://api.cloudm.co.nz/public/v1/events?count=50&offset=0 \
--header 'authorization: Bearer ACCESS_TOKEN' \
--header 'content-type: application/json'
```
Where:
`count`: Indicates the number of records to be returned. E.g the size of a page. 
`offset`: An index starting at 0 indicating where the page begins. E.g with a count of 20, the first page would be index 0, and subsequent pages at 20, 40 etc.
```
It is best practice for a client to always include a "count" parameter, rather than relying on default amount returned by the API.
```