# Getting Started
---
The purpose of this document is to get you started quickly with Blerter’s public API.
Broadly speaking you will need to:
- Register your application to get the credentials needed for authorization.
- Authorize using an OAuth2 flow to get an access token.
- Make requests against the API using the access token you obtained previously.

## Registering Your Application
To register your application with us you will need to provide some information so that we can mark your organization as an authorized application for using our API.
Please send an email to either ​`alisdairb@blerter.com​` or ​`brendonf​@blerter.com​` with your intended ​`REDIRECT_URI`.
The `REDIRECT_URI` is where the OAuth flow will return to upon authorization. E.g. `https://my-app.com​/callback​`.
We will provide you with a `CLIENT_ID` and `CLIENT_SECRET` for your application. **Please take note of these and keep them safe**.
```
Once you have these details and you are familiar with OAuth2 you are good to go. Follow our [Quickstart](./Quickstart) guide to start talking to our API.
```

## Authorizing with OAuth2
Your application will be accessing the API on behalf a user who has already registered with Blerter. As part of the authorization process you must redirect them to a page where they will enter in their username and password and approve your access.
Navigate to:
```
 https://blerter.auth0.com/authorize?audience=user&response_type=code&client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&nonce={CRYPTOGRAPHIC_NONCE}
 ```
Where:
`CLIENT_ID`: Matches the value provided to your organization.
The `STATE` is any value you wish to provide that identifies this request, it will be passed back to your application via `REDIRECT_URI` without being changed.
The `CRYPTOGRAPHIC_NONCE` is (optionally) provided by you and recommended to prevent replay attacks.

Once approved your `REDIRECT_URI` will be called with a code passed to it:
```
https://my-app.com​/callback​?code=CODE=XYZ
```

You will now need to perform a HTTP POST to exchange the `CODE` for access token.
```
curl --request POST \
--url 'https://blerter.auth0.com/oauth/token' \
--header 'content-type: application/json' \
--data '{"grant_type":"authorization_code","client_id":"CLIENT_ID","client_secret": "CLIENT_SECRET","code": "CODE","redirect_uri":"REDIRECT_URI"}'
```

The response should contain your `ACCESS_TOKEN`.
```
{
    "access_token": "eyJz93a...k4laUWw",
    "token_type": "Bearer"
}
```

## Making Requests
The `ACCESS_TOKEN` must be attached as a bearer token in all following requests to the API. E.g.
```
 curl --request GET \
 --url https://api.cloudm.co.nz/public/v1/events \
 --header 'authorization: Bearer ACCESS_TOKEN' \
 --header 'content-type: application/json'
 ```
It will eventually expire (after a couple of hours) but can be used for API calls within this time-frame.

## User-centric API
The Blerter API is user-centric meaning your application is making requests on behalf a Blerter user. This entails:
- Requests made by your application have the same privileges as the user has. 
- Modifications made by your application are indistinguishable from the user making those same requests with another application. E.g they will appear as the author for any records created.

## Versioning
All endpoints include a version number. E.g:`https://api.cloudm.co.nz/public/v1/events` where the `/v1` part of the endpoint indicates that we are using the first major version of the API. (1.x.x) As we update our API these versions may change. This means:
- As newer endpoints become available older ones may become marked as *Deprecated*. 
- You should try to use the most recent version of the API possible. 
- We will endeavour to support older versions of the API as long as is feasible, but they may be eventually removed.
- API versions being *major versions* are a big deal and wont change that quickly. If slight (non-breaking) updates are made to an end-point the version probably wont change.

## Fair Use
- We have the ability to rate limit however do not currently enforce any limiting.
- We ask that people be good api citizens and where possible design their client responsibly.
- If we determine that our api is being abused we will notify that we may impose rate limiting and will work with you to find a solution.

## Next Steps
- Check out the [Quickstart](./Quickstart) guide for an example of how to authorize and make requests.
- Read through [Common Concepts](./Common Concepts) to understand the patterns used in the API.